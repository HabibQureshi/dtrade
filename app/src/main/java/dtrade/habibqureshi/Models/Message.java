package dtrade.habibqureshi.Models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    private String userTagId;
    private String message;
    private String time;

    public String getUserTagId() {
        return userTagId;
    }

    public void setUserTagId(String userTagId) {
        this.userTagId = userTagId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
