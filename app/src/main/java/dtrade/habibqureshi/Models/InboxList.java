package dtrade.habibqureshi.Models;

public class    InboxList {
    private String user;
    private String time;
    private String message;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (((InboxList)obj).getUser().equals(this.getUser()))
            return true;
        else return false;
    }
}
