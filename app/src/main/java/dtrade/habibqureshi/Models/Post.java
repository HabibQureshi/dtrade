package dtrade.habibqureshi.Models;

import java.util.Arrays;
import java.util.List;

public class Post {

    private String postId;
    private List<String> clarity;
    private List<String> color;
    private List<String> size;
    private String comments;
    private String createdDate;
    private String shape;
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public List<String> getClarity() {
        return clarity;
    }

    public void setClarity(List<String> clarity) {
        this.clarity = clarity;
    }

    public List<String> getColor() {
        return color;
    }

    public void setColor(List<String> color) {
        this.color = color;
    }

    public List<String> getSize() {
        return size;
    }

    public void setSize(List<String> size) {
        this.size = size;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId='" + postId + '\'' +
                ", clarity='" + clarity + '\'' +
                ", color=" + color +
                ", size=" + size +
                ", comments='" + comments + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", shape='" + shape + '\'' +
                '}';
    }
}
