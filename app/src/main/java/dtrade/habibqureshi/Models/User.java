package dtrade.habibqureshi.Models;

public class User {
    private String userTagId;
    private String createdDate;
    private String enable;
    private String password;
    private String username;
    private String picture;
    private String contact;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUserTagId() {
        return userTagId;
    }

    public void setUserTagId(String userTagId) {
        this.userTagId = userTagId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "User{" +
                "userTagId='" + userTagId + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", enable='" + enable + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", picture='" + picture + '\'' +
                '}';
    }
}
