package dtrade.habibqureshi.Network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import dtrade.habibqureshi.Interfaces.OnJsonResponse;
import dtrade.habibqureshi.Utils.M;

/**
 * Created by HabibQureshi on 10/18/2017.
 */

public class GetJsonResponse extends AsyncTask<String, Void, String> {
    OnJsonResponse jsonResponse;
    int RequestType;
    String json;

    public GetJsonResponse(OnJsonResponse jsonResponse,int RequestType,String json) {
        this.json=json;
        this.jsonResponse = jsonResponse;
        this.RequestType=RequestType;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        this.jsonResponse.onJsonResponse(response,RequestType);
    }

    @Override
    protected String doInBackground(String... params) {
        URL ulrn = null;
        HttpURLConnection con=null;
        try {
            ulrn = new URL(params[0]);
            M.l("doInBackground:",ulrn.toString());
            con = (HttpURLConnection) ulrn.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            con.setRequestProperty("Accept","application/json");
            con.setRequestProperty("Authorization","key=AIzaSyDD2MkXyjmXzFLdkLCc9y8Zo-sR8Ux9ZVQ");
            con.setDoOutput(true);
            con.setDoInput(true);
            DataOutputStream os = new DataOutputStream(con.getOutputStream());
            os.writeBytes(json.toString());

            os.flush();
            os.close();
            Log.i("STATUS", String.valueOf(con.getResponseCode()));
            Log.i("MSG" , con.getResponseMessage());



//            OutputStream os = con.getOutputStream();
//            os.write(this.json.getBytes("UTF-8"));
//            os.close();
//            InputStream is = con.getInputStream();
//            BufferedReader in = new BufferedReader(
//                    new InputStreamReader(is));
//            String inputLine;
//            StringBuffer response = new StringBuffer();
//            while ((inputLine = in.readLine()) != null) {
//                response.append(inputLine);
//            }
//            in.close();
            con.disconnect();
            return con.getResponseMessage();


        } catch (Exception e) {

            e.printStackTrace();
            M.l(e.getMessage() + " exception");
            try {
                M.l("code "+con.getResponseCode());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }
}