package dtrade.habibqureshi.Service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import dtrade.habibqureshi.Utils.M;

public class FirebaseMessage extends FirebaseInstanceIdService {
    public FirebaseMessage() {
        super();
        M.l("FirebaseMessage");
    }

    private static final String TAG = "MyFirebaseIIDService";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
    }
}
