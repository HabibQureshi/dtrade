/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dtrade.habibqureshi.Service;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.Utils.M;


public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]

    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     *
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        M.l("onMessageReceived");
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            }

        if (remoteMessage.getNotification() != null) {
            M.l("Message Notification Body: " + remoteMessage.getNotification().getBody());
            Intent intent = new Intent("notification");
            intent.putExtra("message", remoteMessage.getNotification().getBody());
            intent.putExtra("tittle", remoteMessage.getNotification().getTitle());
            intent.putExtra("user", remoteMessage.getData().get("user"));
            intent.putExtra("s",remoteMessage.getData().get("s"));
            intent.putExtra("type",remoteMessage.getData().get("type"));
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        }
        else
            M.l("Message notification is null");

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}