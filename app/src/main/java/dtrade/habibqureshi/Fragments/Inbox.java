package dtrade.habibqureshi.Fragments;


import android.app.Notification;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import dtrade.habibqureshi.Adapters.InboxAdapter;
import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Interfaces.OnClickInterface;
import dtrade.habibqureshi.Models.InboxList;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;

/**
 * A simple {@link Fragment} subclass.
 */
public class Inbox extends Fragment implements ChildEventListener,View.OnClickListener{

    private View layout;
    private HomeActivity activity;
    private FirebaseDatabase db;
    private LocalStorage storage;
    private List<InboxList> inbox;
    private RecyclerView recyclerView;
    private InboxAdapter inboxAdapter;
    private DatabaseReference myRef;
    private long count = -1;
    private ImageView back;

    public Inbox() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_inbox, container, false);
        findViews();
        init();
        if(activity.check_network())
        {
            activity.showDialog("Loading....");

        }

        else {
            activity.cancelDialog();
            M.t(activity,"Enable internet");
        }
        return layout;
    }



    private void init() {
        this.db = FirebaseDatabase.getInstance();
        this.storage = new LocalStorage(activity);
        this.inbox = new ArrayList<>();
        inboxAdapter = new InboxAdapter(new ArrayList<InboxList>());
        recyclerView.setAdapter(inboxAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.addOnItemTouchListener(new Navigation.RecyclerTouchListener(activity, recyclerView, new OnClickInterface() {
            @Override
            public void drawerOnClick(View view, int position) {
                Chat chat = new Chat();
                Bundle bundle = new Bundle();
                bundle.putString("user",PostSaving.inbox.get(position).getUser());
                chat.setArguments(bundle);
                activity.changeFragment(chat,PostSaving.users.get(PostSaving.inbox.get(position).getUser()).getUsername(),PostSaving.users.get(PostSaving.inbox.get(position).getUser()).getUsername());
            }
        }));
        this.myRef  = this.db.getReference(DocumentNames.inbox+"_"+storage.getTagID());
        this.myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null)
                    activity.cancelDialog();
                else count = dataSnapshot.getChildrenCount();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                activity.cancelDialog();

            }
        });
        this.myRef.addChildEventListener(this);
        this.activity.setCurrentFragment(this);
        this.back.setOnClickListener(this);

    }

    private void findViews() {
        recyclerView = layout.findViewById(R.id.inbox);
        back = layout.findViewById(R.id.back);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity= (HomeActivity) context;
    }


    private void showInbox() {
        sortInbox(PostSaving.inbox);
        inboxAdapter.setInboxLists(PostSaving.inbox);
        activity.cancelDialog();
        inboxAdapter.notifyDataSetChanged();

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        M.l("inbox onChildAdded");
        inbox.add(dataSnapshot.getValue(InboxList.class));
        PostSaving.inbox=inbox;
        showInbox();



    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        M.l("inbox onChildChanged");
        inbox.remove(dataSnapshot.getValue(InboxList.class));
        inbox.add(dataSnapshot.getValue(InboxList.class));
        PostSaving.inbox=inbox;
        showInbox();

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public void sortInbox(List<InboxList> inbox){
        Collections.sort(inbox, new Comparator<InboxList>() {
            @Override
            public int compare(InboxList o1, InboxList o2) {
                return o2.getTime().compareTo(o1.getTime());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                activity.removeFragment();
                break;
        }
    }
}
