package dtrade.habibqureshi.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SnapshotMetadata;

import org.w3c.dom.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dtrade.habibqureshi.Adapters.PostAdapter;
import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Interfaces.OnClickInterface;
import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements View.OnClickListener ,OnCompleteListener<QuerySnapshot> {
    private View layout;
    private Button post;
    private HomeActivity activity;
    private RecyclerView recyclerView;
    private FirebaseFirestore db;
    private LocalStorage storage;
    private PostAdapter postAdapter;
    private static List<Post> usersPosts;
    private static HashMap<String,User> users;
    private int totalPost,totalUsers;
    private SwipeRefreshLayout refreshLayout;
    private Spinner shapes;
    private LinearLayout size;
    private boolean isDataFilter=false;
    private ImageView shapesImage;


    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout= inflater.inflate(R.layout.fragment_home, container, false);
        setRetainInstance(true);
        findViews();
        init();
        if(activity.check_network())
        {
            activity.showDialog("Loading....");
            fetchAllPost();
        }

        else M.t(activity,"Enable internet");
        return layout;
    }

    private void fetchAllPost() {
        if(PostSaving.userPosts==null|| PostSaving.userPosts.size()==0){
            M.l("post or user null");
            db.collection(DocumentNames.usersPost).get().addOnCompleteListener(this);
            db.collection(DocumentNames.users).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if(task.isSuccessful()){
                        List<DocumentSnapshot> documents = task.getResult().getDocuments();
                        totalUsers = documents.size();
                            for (DocumentSnapshot document : documents) {
                                M.l("getting user with tag id : "+document.getId());
                                db.collection(DocumentNames.users).document(document.getId()).get().addOnCompleteListener(userListner);
                                }
                    }
                }
            });
        }
        else{
            usersPosts=LocalStorage.getPosts();
            M.l("calling show post from 113");
            showPost();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof HomeActivity)
            activity=(HomeActivity) context;
    }

    private void init(){
        post.setOnClickListener(this);
        this.db = FirebaseFirestore.getInstance();
        this.storage = new LocalStorage(activity);
        postAdapter=new PostAdapter(false,null);
        recyclerView.setAdapter(postAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.addOnItemTouchListener(new Navigation.RecyclerTouchListener(activity, recyclerView, new OnClickInterface() {
            @Override
            public void drawerOnClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putInt("post",position);
                dtrade.habibqureshi.Fragments.Post post= new dtrade.habibqureshi.Fragments.Post();
                post.setArguments(bundle);
                activity.changeFragment(post,"Post","Post");

            }
        }));
        usersPosts = new ArrayList<>();
        users = new HashMap<>();
        refreshLayout.setOnRefreshListener(refreshListener);
        final ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter.createFromResource(activity, R.array.shapes, R.layout.layout);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shapes.setAdapter(staticAdapter);
        shapes.setOnItemSelectedListener(shapeAdapter);
        size.setOnClickListener(this);
        this.activity.setCurrentFragment(this);
        this.shapesImage.setOnClickListener(this);


    }

    private void getPostWithShape(String s) {
        List<Post> shapeFilter = new ArrayList<>();
        for (Post post:PostSaving.userPosts){
            if(post.getShape().equals(s))
                shapeFilter.add(post);
        }
        if(shapeFilter.size()==0)
            M.t(activity,"No data found");
        else showFilterData(shapeFilter);
    }

    private void findViews(){
        post = layout.findViewById(R.id.post);
        recyclerView = layout.findViewById(R.id.usersPosts);
        refreshLayout = layout.findViewById(R.id.refresh);
        shapes = layout.findViewById(R.id.shapes);
        size = layout.findViewById(R.id.sizeFilter);
        shapesImage = layout.findViewById(R.id.shapesImage);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.post:
                activity.changeFragment(new NewPost(),"NewPost","NewPost");
                break;
            case R.id.sizeFilter:
                showSizeSearchDailog();
                break;
            case R.id.shapesImage:
                shapes.performClick();
                break;
        }
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        if(task.isSuccessful()){
            List<DocumentSnapshot> documents = task.getResult().getDocuments();
            totalPost=documents.size();
            totalUsers= documents.size();
            if(documents.size()==0){
                activity.cancelDialog();
                M.t(activity,"Nothing to show");
            }
            else{
                for (DocumentSnapshot document : documents) {
                    M.l("getting user with tag id : "+document.getId());
                    db.collection(DocumentNames.usersPost).document(document.getId()).collection(DocumentNames.post).get()
                            .addOnCompleteListener(postListener);
                }
            }

        }

    }
    private void showPost() {
        M.l("showPosy");
        activity.cancelDialog();
        if(PostSaving.userPosts == null)
            PostSaving.userPosts= usersPosts;
        if(PostSaving.users == null)
            PostSaving.users = users;
        postAdapter.setPosts(PostSaving.userPosts,PostSaving.users);
        postAdapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);
    }

    private void showFilterData(List<Post> posts){
        postAdapter.setPosts(posts,PostSaving.users);
        postAdapter.notifyDataSetChanged();
        isDataFilter=true;
    }


    private OnCompleteListener<DocumentSnapshot> userListner = new OnCompleteListener<DocumentSnapshot>() {


        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if(task.isSuccessful()){
                if(task.getResult().exists()){
                    DocumentSnapshot documentSnapshot = task.getResult();
                    User user = documentSnapshot.toObject(User.class);
                    M.l("user arrived :"+user.getUserTagId());
                    users.put(documentSnapshot.getId(),user);
                    totalUsers--;
                    if(totalUsers<=0&&totalPost<=0){
                        Collections.sort(usersPosts, new Comparator<Post>() {
                            @Override
                            public int compare(Post o1, Post o2) {
                                return o2.getCreatedDate().compareTo(o1.getCreatedDate());
                            }
                        });
                        M.l(totalPost+" " +totalUsers);
                        deleteMoreThen7DaysOldPosts();
                        M.l("calling show post from 259");
                        showPost();
                    }
            }else M.l("user not exist");

        }else M.t(activity,"Check internet");
    }
    };
  private OnCompleteListener<QuerySnapshot> postListener =  new OnCompleteListener<QuerySnapshot>() {
        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if(task.isSuccessful()){
                QuerySnapshot queryDocumentSnapshots=task.getResult();
                List<DocumentSnapshot> documentsList = queryDocumentSnapshots.getDocuments();
                for(DocumentSnapshot document1 : documentsList){
                    SnapshotMetadata snapshotMetadata=document1.getMetadata();
                    snapshotMetadata.toString();
                    DocumentReference documentReference=document1.getReference();
                    DocumentReference documentReference1=documentReference.getParent().getParent();
                    M.l(documentReference1.getId());
                    Post post = document1.toObject(Post.class);
                    post.setPostId(document1.getReference().getId());
                    post.setUser(document1.getReference().getParent().getParent().getId());
                    usersPosts.add(post);
                    }
                totalPost--;
                if(totalPost<=0 && totalUsers <=0){
                    Collections.sort(usersPosts, new Comparator<Post>() {
                        @Override
                        public int compare(Post o1, Post o2) {
                            return o2.getCreatedDate().compareTo(o1.getCreatedDate());
                        }
                    });
                    deleteMoreThen7DaysOldPosts();
                    M.l(totalPost+" " +totalUsers);
                    M.l("calling show post from 293");
                    showPost();
                }
            }
        }
    };

    private void deleteMoreThen7DaysOldPosts() {
        for (int i = usersPosts.size()-1;i>=0;i--)
        {
            if(isPostExpire(usersPosts.get(i)))
            {
                deletePost(usersPosts.get(i));
                usersPosts.remove(i);

            }
        }
    }

    private boolean isPostExpire(Post post) {
        M.l("checking expire date");
        try {
            Date postDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(post.getCreatedDate());
            Date today = Calendar.getInstance().getTime();
            long diff =(today.getTime()-postDate.getTime());
            float days = (diff / (1000*60*60*24));
            M.l("difference in days "+days);
            if(days>7)
                return true;
            else return false;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }


    }

    private void deletePost(final Post post) {
        M.l("Post is going to delete because it is expire",post.toString());
        this.db.collection(DocumentNames.usersPost).document(post.getUser()).collection(DocumentNames.post).document(post.getPostId())
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        M.t(activity,"Some thing went wrong");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        M.l("Deleted");

                    }
                });

    }

    private void showSizeSearchDailog(){
        final Dialog d = new Dialog(activity);
        d.setContentView(R.layout.size_selection);
        final EditText lowerSize, upperSize;
        Button search;
        lowerSize = d.findViewById(R.id.lowerLimit);
        upperSize = d.findViewById(R.id.uperLimit);
        search = d.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateSize(lowerSize.getText().toString(),upperSize.getText().toString()))
                {
                    List<Post> shapeFilter = new ArrayList<>();
                    for (Post post:PostSaving.userPosts){
                        if((Float.parseFloat(post.getSize().get(0)) >= Float.parseFloat(lowerSize.getText().toString())) &&
                                (Float.parseFloat(post.getSize().get(1)) <= Float.parseFloat(upperSize.getText().toString())))
                            shapeFilter.add(post);
                    }
                    if(shapeFilter.size()==0)
                        M.t(activity,"No data found");
                    else showFilterData(shapeFilter);
                }
                d.dismiss();
            }
        });
        d.show();
    }
    private boolean validateSize(String low,String up) {
        String lowerLimit = low;
        String upperLimit = up;
        if(lowerLimit.isEmpty()) {
            M.t(activity,"Please enter lower limit");
            return false;
        }
        if(upperLimit.isEmpty()) {
            M.t(activity,"Please enter upper limit");
            return false;
        }

        double lower = Double.parseDouble(lowerLimit);
        double upper  = Double.parseDouble(upperLimit);

        if(lower<=upper){
            if(lower>0.01)
            {
                if(upper<=100)
                    return true;
                else
                    M.t(activity,"upper limit should be less or equal to 100");
            }
            else
                M.t(activity,"lower limit should be greater or equal to 0.01");
        }
        else
            M.t(activity,"lower size can't greater then upper size");
        return false;
    }
    private AdapterView.OnItemSelectedListener shapeAdapter= new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            M.l("item selected");
            if(!shapes.getSelectedItem().toString().equals("All"))
                getPostWithShape(shapes.getSelectedItem().toString());
            else if(PostSaving.userPosts!=null)
                showPost();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            M.l("nothing selected");

        }
    };
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            PostSaving.userPosts = new ArrayList<>();
            usersPosts = PostSaving.userPosts;
            PostSaving.users = new HashMap<>();
            users = PostSaving.users;
            fetchAllPost();

        }
    };

    public boolean isDataFilter() {
        return isDataFilter;
    }

    public void setDataFilter(boolean dataFilter) {
        isDataFilter = dataFilter;
    }
    public void showAllPost(){
        this.showPost();
    }

}
