package dtrade.habibqureshi.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.InputStream;

import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.util;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfile extends Fragment implements View.OnClickListener , OnCompleteListener<DocumentSnapshot> {

    private ImageView userPicture;
    private TextView uploadImage,tagId;
    private EditText phone,password,username;
    private View layout;
    private HomeActivity activity;
    private FirebaseFirestore db;
    private LocalStorage storage;
    private String base64Image;
    private Button update;
    private ImageView back;





    public MyProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout=  inflater.inflate(R.layout.fragment_my_profile, container, false);
        setRetainInstance(true);
        findViews();
        init();
        showUserInfo();
        return layout;
    }

    private void showUserInfo() {
        if (LocalStorage.user==null){
            activity.showDialog("Fetching....");
            DocumentReference documentReference= db.collection(DocumentNames.users).document(storage.getTagID());
            documentReference.get().addOnCompleteListener(this);

        }
        else
            displayInfo(LocalStorage.user);
    }

    private void displayInfo(User user) {
        this.tagId.setText(user.getUserTagId());
        this.password.setText(user.getPassword());
        this.phone.setText(user.getContact());
        this.username.setText(user.getUsername());
        if(user.getPicture()!=null){
            this.userPicture.setImageBitmap(util.convertBase64ToImage(user.getPicture()));

        }

    }



    private void init() {
        //uploadImage.setOnTouchListener(touchListener);
        this.uploadImage.setOnClickListener(this);
        this.db = FirebaseFirestore.getInstance();
        this.storage= new  LocalStorage(activity);
        this.update.setOnClickListener(this);
        this.activity.setCurrentFragment(this);
        this.back.setOnClickListener(this);

    }

    private void findViews() {
        userPicture=layout.findViewById(R.id.userPhoto);
        uploadImage=layout.findViewById(R.id.selectPhoto);
        tagId = layout.findViewById(R.id.userTag);
        phone = layout.findViewById(R.id.userPhone);
        password=layout.findViewById(R.id.password);
        update = layout.findViewById(R.id.update);
        username = layout.findViewById(R.id.username);
        back = layout.findViewById(R.id.back);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.selectPhoto:
                pickImage();
                break;
            case R.id.update:
                updateUser();
                break;
            case R.id.back:
                activity.removeFragment();
                break;
        }
    }

    private void updateUser() {
        if(isDataUpdated()){
            activity.showDialog("Updating user...");
            updateUserData();
        }
    }

    private void updateUserData() {
        this.db.collection(DocumentNames.users).document(LocalStorage.user.getUserTagId()).set(LocalStorage.user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        activity.cancelDialog();
                        M.l("Updated");

                    }
                });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                InputStream inputStream = activity.getContentResolver().openInputStream(data.getData());
                base64Image = Base64.encodeToString(IOUtils.toByteArray(inputStream),Base64.DEFAULT);
                this.userPicture.setImageBitmap(util.convertBase64ToImage(base64Image));
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (HomeActivity)context;
    }

    @Override
    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
        activity.cancelDialog();
        if(task.isSuccessful()){
                DocumentSnapshot documentSnapshot = task.getResult();
                User user  = documentSnapshot.toObject(User.class);
                LocalStorage.user=user;
                displayInfo(user);

        }else M.t(activity,"Request Failed, check your connection");

    }

    public boolean isDataUpdated() {
        boolean response=false;
        if(validData()) {
            if(LocalStorage.user.getPicture()==null){
                LocalStorage.user.setPicture(base64Image);
                response = true;
            }
            else
            if (!LocalStorage.user.getPicture().equals(base64Image)) {
                LocalStorage.user.setPicture(base64Image);
                response = true;
            }
            if (!LocalStorage.user.getPassword().equals(password.getText().toString())) {
                LocalStorage.user.setPassword(password.getText().toString());
                response = true;
            }
            if(LocalStorage.user.getContact()==null){
                LocalStorage.user.setContact(phone.getText().toString());
                response = true;
            }else
            if (!LocalStorage.user.getContact().equals(phone.getText().toString())) {
                LocalStorage.user.setContact(phone.getText().toString());
                response = true;
            }
            else if(!LocalStorage.user.getUsername().equals(username.getText().toString())){
                LocalStorage.user.setUsername(username.getText().toString());
                response = true;

            }
        }
        

        return response;
    }

    private boolean validData() {
        if(phone.getText().toString().isEmpty()){
            M.t(activity,"Phone number can't be empty");
            return false;
        }
        if(password.getText().toString().isEmpty()){
            M.t(activity,"Password  can't be empty");
            return false;
        }
        return true;
        
    }
}
