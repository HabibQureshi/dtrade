package dtrade.habibqureshi.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;

/**
 * A simple {@link Fragment} subclass.
 */
public class Setting extends Fragment implements View.OnClickListener{
    private CheckBox post,message;
    private View layout;
    private HomeActivity activity;
    private LocalStorage storage;
    private ImageView back;


    public Setting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_setting, container, false);
        findViews();
        init();
        return layout;
    }

    private void init() {
        storage = new LocalStorage(activity);
        post.setChecked(storage.getPostAlert());
        message.setChecked(storage.getMessageAlert());
        this.back.setOnClickListener(this);
    }

    void findViews(){
        post = layout.findViewById(R.id.postNotofication);
        message = layout.findViewById(R.id.messageNotofication);
        back = layout.findViewById(R.id.back);
    }

    public void settings(View v){
        CheckBox checkBox;
        switch (v.getId()){
            case R.id.postNotofication:
                checkBox = (CheckBox)v;
                if (checkBox.isChecked()){
                    activity.subscribeToPostNotification();
                    storage.setPostAlert(true);
                }


                else {
                    activity.unSubscribeToPostNotification();
                    storage.setPostAlert(false);
                }
                break;
            case R.id.messageNotofication:
                 checkBox = (CheckBox)v;
                if (checkBox.isChecked()){
                    storage.setMessageAlert(true);
                    activity.subscribeToMessageNotification();
                }

                else{
                    storage.setMessageAlert(false);
                    activity.unSubscribeToMessageNotification();
                }
                break;

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(HomeActivity)context;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                activity.removeFragment();
                break;
        }
    }
}
