package dtrade.habibqureshi.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dtrade.habibqureshi.Adapters.ChatAdapter;
import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentFieldsNames;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Models.Message;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;
import dtrade.habibqureshi.Utils.util;

/**
 * A simple {@link Fragment} subclass.
 */
public class Chat extends Fragment implements ChildEventListener,View.OnClickListener{


    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private DatabaseReference myInbox;
    private DatabaseReference userInbox;
    private RecyclerView chats;
    private EditText text;
    private Button send;
    private View layout;
    private HomeActivity activity;
    private String user;
    private LocalStorage storage;
    private List<Message> messages;
    private ChatAdapter chatAdapter;
    private List<String> inbox;
    private ImageView photo,back;
    private TextView username,tagid,phone;
    private long count=-1;


    public Chat() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout =  inflater.inflate(R.layout.fragment_chat, container, false);
        findViews();
        init();
        return layout;
    }

    private void findViews() {
        chats=layout.findViewById(R.id.chat);
        text=layout.findViewById(R.id.usermessage);
        send=layout.findViewById(R.id.send);
        photo = layout.findViewById(R.id.userPhoto);
        username = layout.findViewById(R.id.username);
        phone = layout.findViewById(R.id.phone);
        tagid = layout.findViewById(R.id.tagid);
        back = layout.findViewById(R.id.back);
        }

    private void init() {

        this.inbox = new ArrayList<>();
        this.storage = new LocalStorage(activity);
        this.send.setOnClickListener(this);
        this.user = getArguments().getString("user");
        this.database = FirebaseDatabase.getInstance();
        this.myRef = database.getReference(DocumentNames.chat+getref());
        this.myInbox= database.getReference(DocumentNames.inbox+"_"+storage.getTagID());
        this.userInbox = database.getReference(DocumentNames.inbox+"_"+user);
        this.messages = new ArrayList<>();
        this.myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null)
                    activity.cancelDialog();
                }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                activity.cancelDialog();

            }
        });
        sortMessages(messages);
        this.chatAdapter = new ChatAdapter(messages,activity,user);
        this.chats.setAdapter(chatAdapter);
        this.chats.setLayoutManager(new LinearLayoutManager(activity));
        this.myRef.addChildEventListener(this);
        this.activity.showDialog("Fetching...");
        this.activity.setCurrentFragment(this);
        this.tagid.setText(user);
        this.back.setOnClickListener(this);
        if(PostSaving.users !=null){
            phone.setText(PostSaving.users.get(user).getContact());
            username.setText(PostSaving.users.get(user).getUsername());
            String pic= PostSaving.users.get(user).getPicture();
            if(pic != null)
                photo.setImageBitmap(util.convertBase64ToImage(pic));
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity=(HomeActivity)context;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.send:
                if(text.getText().toString().length()>0){
                    this.sendMessage();
                    text.setText("");
                }
                break;
            case R.id.back:
                activity.removeFragment();
                break;
        }

    }

    private void sendMessage() {
        Message message = new Message();
        message.setMessage(text.getText().toString());
        message.setTime(util.getCurrentTime());
        message.setUserTagId(storage.getTagID());
        Map<String,Object> map=new HashMap<>();
        map.put(util.getCurrentTime(),message);
        myRef.updateChildren(map);
        this.updateBothInbox();
        this.activity.sendNotification(user,"New Message","newmessage","message from "+storage.getTagID(),storage.getTagID());

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        M.l("child added");
        Message message = dataSnapshot.getValue(Message.class);
        this.messages.add(message);
        if(count==messages.size()|| count==-1){ //if all message arrive dont change data set on each message
            this.activity.cancelDialog();
            this.chatAdapter.notifyDataSetChanged();
            chats.smoothScrollToPosition(messages.size()-1);
            }
            else if(count<messages.size()){ // new message count greater then all pervious messages
            this.chatAdapter.notifyDataSetChanged();
            chats.smoothScrollToPosition(messages.size()-1);
        }






    }

    private void updateBothInbox() {
        HashMap<String,Object> addUser = new HashMap<>();
        HashMap<String,Object> userData = new HashMap<>();
        userData.put(DocumentFieldsNames.user,user);
        userData.put(DocumentFieldsNames.time,util.getCurrentTime());
        userData.put(DocumentFieldsNames.message,text.getText().toString());
        addUser.put(user,userData);
        this.myInbox.updateChildren(addUser);
        addUser = new HashMap<>();
        userData.put("user",storage.getTagID());
        addUser.put(storage.getTagID(),userData);
        userInbox.updateChildren(addUser);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        M.l(dataSnapshot.toString());

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        M.l(databaseError.toString());
        activity.cancelDialog();

    }

    public String getUser(){
        return user;
    }

    private String getref() {
        M.l(user+"");
        M.l(storage.getTagID()+"");
        if(Integer.parseInt(user) > Integer.parseInt(storage.getTagID()))
            return user+"_"+storage.getTagID();
        else return storage.getTagID()+"_"+user;
    }




    private void sortMessages(List<Message> messages){
        Collections.sort(messages, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o2.getTime().compareTo(o1.getTime());
            }
        });
    }
}
