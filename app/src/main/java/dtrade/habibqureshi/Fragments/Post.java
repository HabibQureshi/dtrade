package dtrade.habibqureshi.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;

import dtrade.habibqureshi.Adapters.PostAdapter;
import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.Ativities.MainActivity;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.PostSaving;
import dtrade.habibqureshi.Utils.util;

/**
 * A simple {@link Fragment} subclass.
 */
public class Post extends Fragment implements View.OnClickListener{
    private TextView shape, size, color, clarity, comments, postTime, userName , userPhone , userTagId , postData;
    private ImageView userPic,back;
    private View layout;
    private HomeActivity activity;
    private Button contact;
    private LocalStorage storage;
    private int post;
    


    public Post() {

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout= inflater.inflate(R.layout.fragment_post, container, false);
        findViews();
        init();
        return layout;
    }

    private void init() {
        this.storage= new LocalStorage(activity);
        this.post=getArguments().getInt("post");
        this.contact.setOnClickListener(this);
        this.setPostData(post);
        this.activity.setCurrentFragment(this);
        this.back.setOnClickListener(this);

    }

    private void setPostData(int position) {
        dtrade.habibqureshi.Models.Post userPost=PostSaving.userPosts.get(position);
        User user = PostSaving.users.get(PostSaving.userPosts.get(position).getUser());
        postData.setText(Html.fromHtml(userPost.getShape()+" | " + userPost.getSize().get(0)+"ct - "+userPost.getSize().get(1)+"ct | " +
                util.getListAsString(userPost.getColor()) + " | " + util.getListAsString(userPost.getClarity()) +"<br><span style=\"color:red;\">" + userPost.getComments()+"</span>"));
//        shape.setText(userPost.getShape());
//        comments.setText(userPost.getComments());
//        size.setText(userPost.getSize().get(0)+" to "+userPost.getSize().get(1));
//        color.setText(util.getListAsString(userPost.getColor()));
//        clarity.setText(util.getListAsString(userPost.getClarity()));
//        userName.setText(user.getUsername());
        postTime.setText(userPost.getCreatedDate());
        userTagId.setText(user.getUserTagId());
        if(user.getContact() == null || user.getContact().equals(""))
            userPhone.setText("n/a");
        else
        userPhone.setText(user.getContact());

        if(user.getPicture()!=null)
                userPic.setImageBitmap(util.convertBase64ToImage(user.getPicture()));
        else
            userPic.setImageResource(R.drawable.default_user);

        if(user.getUserTagId().equals(storage.getTagID()))
            contact.setVisibility(View.GONE);


        }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (HomeActivity)context;
    }
    
    private void findViews(){

        postData =  layout.findViewById(R.id.postdata);
//        shape = layout.findViewById(R.id.shape);
//        size = layout.findViewById(R.id.size);
//        color = layout.findViewById(R.id.color);
//        clarity = layout.findViewById(R.id.clarity);
//        comments = layout.findViewById(R.id.comments);
        postTime = layout.findViewById(R.id.postTime);
        userName = layout.findViewById(R.id.username);
        userPic = layout.findViewById(R.id.userPic);
        contact = layout.findViewById(R.id.contact);
        userPhone = layout.findViewById(R.id.userPhone);
        userTagId = layout.findViewById(R.id.tagid);
        back = layout.findViewById(R.id.back);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contact:
                Chat chat = new Chat();
                Bundle bundle = new Bundle();
                bundle.putString("user",PostSaving.users.get(PostSaving.userPosts.get(post).getUser()).getUserTagId());
                chat.setArguments(bundle);
                activity.changeFragment(chat,PostSaving.users.get(PostSaving.userPosts.get(post).getUser()).getUsername(),PostSaving.users.get(PostSaving.userPosts.get(post).getUser()).getUsername());
                break;
            case R.id.back:
                activity.removeFragment();
                break;
        }

    }
}
