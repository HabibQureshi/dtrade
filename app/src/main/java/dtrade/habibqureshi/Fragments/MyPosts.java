package dtrade.habibqureshi.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Document;

import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dtrade.habibqureshi.Adapters.DrawerListAdapter;
import dtrade.habibqureshi.Adapters.PostAdapter;
import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPosts extends Fragment implements OnCompleteListener<QuerySnapshot> , View.OnClickListener{
    private View layout;
    private HomeActivity activity;
    private FirebaseFirestore db;
    private LocalStorage storage;
    private RecyclerView recyclerView;
    private PostAdapter postAdapter;
    private Button post;
    private ImageView back;



    public MyPosts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_my_post, container, false);
        setRetainInstance(true);
        findViews();
        init();
        if(activity.check_network())
            fetchUserPost();
        else M.t(activity,"Enable internet");
        return layout;
    }

    private void fetchUserPost() {

        if(PostSaving.myPost==null||PostSaving.myPost.size()==0) {
            activity.showDialog("Fetching your post...");
            this.db.collection(DocumentNames.usersPost).document(storage.getTagID()).collection(DocumentNames.post).get()
                    .addOnCompleteListener(this);
        }

        else showPost(PostSaving.myPost);
    }

    private void init() {
        this.db = FirebaseFirestore.getInstance();
        this.storage = new LocalStorage(activity);
        postAdapter=new PostAdapter(true,this);
        recyclerView.setAdapter(postAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        post.setOnClickListener(this);
        this.activity.setCurrentFragment(this);
        this.back.setOnClickListener(this);

    }

    private void findViews() {
        recyclerView = layout.findViewById(R.id.postRecycle);
        post = layout.findViewById(R.id.newPost);
        back = layout.findViewById(R.id.back);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof HomeActivity)
            activity=(HomeActivity)context;
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        activity.cancelDialog();
        if(task.isSuccessful()){
           QuerySnapshot queryDocumentSnapshots = task.getResult();
           List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
           if(documents.size()>0){
                mapPostsData(documents);
           }
        }

    }

    private void mapPostsData(List<DocumentSnapshot> documents) {
        List<Post> posts = new ArrayList<>();
        for (DocumentSnapshot documentSnapshot : documents)
        {
            Post post = documentSnapshot.toObject(Post.class);
            post.setPostId(documentSnapshot.getId());
            posts.add(post);

        }
        showPost(posts);
    }

    private void showPost(List<Post> posts) {
        if(PostSaving.myPost==null)
            PostSaving.myPost=posts;
        Collections.sort(PostSaving.myPost, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getCreatedDate().compareTo(o1.getCreatedDate());
            }
        });
       postAdapter.setPosts(PostSaving.myPost,PostSaving.users);
       postAdapter.notifyDataSetChanged();

    }

    private void deletePost(final String postID) {
        M.l(postID);
        this.db.collection(DocumentNames.usersPost).document(storage.getTagID()).collection(DocumentNames.post).document(postAdapter.getPostId(Integer.parseInt(postID)))
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        M.t(activity,"Some thing went wrong");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        activity.cancelDialog();
                        postAdapter.removeItem(Integer.parseInt(postID));
                        postAdapter.notifyItemRemoved(Integer.parseInt(postID));
                        PostSaving.userPosts=PostSaving.myPost=null;
                        M.t(activity,"Deleted");

                    }
                });

    }

    @Override
    public void onClick(View v) {
        final View image=v;
        switch (v.getId()){
            case R.id.deletePost:
                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Are you sure you want to delete this post?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                activity.showDialog("Deleting.....");
                                deletePost(image.getContentDescription().toString());
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            case R.id.newPost:
                activity.changeFragment(new NewPost(),"NewPost","NewPost");
                break;
            case R.id.back:
                activity.removeFragment();
                break;
        }
    }
}
