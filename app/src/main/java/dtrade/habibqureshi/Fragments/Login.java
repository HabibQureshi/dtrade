package dtrade.habibqureshi.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.okhttp.internal.Util;

import java.util.HashMap;

import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentFieldsNames;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Ativities.MainActivity;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.util;

public class Login extends Fragment implements
        OnFailureListener,OnCompleteListener<DocumentSnapshot>,View.OnClickListener{
    private View layout;
    private MainActivity mainActivity;
    private FirebaseFirestore db;
    private EditText userID,passward;
    private Button loginButton;
    private ProgressDialog progress;
    private LocalStorage storage;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout =  inflater.inflate(R.layout.login_layout, container, false);
        findViews();
        init();
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity= (MainActivity)context;
    }
    private void init(){
        this.db = FirebaseFirestore.getInstance();
        loginButton.setOnClickListener(this);
        progress=new ProgressDialog(mainActivity);
        storage = new LocalStorage(mainActivity);

    }
    private void findViews(){
        userID=layout.findViewById(R.id.tagid);
        passward=layout.findViewById(R.id.password);
        loginButton =layout.findViewById(R.id.login);

    }

    @Override
    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
        this.cancelDialog();
        if(task.isSuccessful()){
            if(task.getResult().exists()){
                DocumentSnapshot documentSnapshot = task.getResult();
                User user  = documentSnapshot.toObject(User.class);
                if(user.getUserTagId().equals(userID.getText().toString()) && user.getPassword().equals(passward.getText().toString()))
                {
                    if(user.getEnable().equals("true")){
                        storage.setLogin(true);
                        storage.setTagID(userID.getText().toString().trim());
                        LocalStorage.user=user;
                        mainActivity.startHomeActivity();
                    }
                    else M.t(mainActivity,"User is disabled");

                }
                else
                M.t(mainActivity,"Incorrect password");

            }else showSignUpPopUp();
        }else M.t(mainActivity,"Request Failed, check your connection");
    }

    private void showSignUpPopUp() {
        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("This Tag ID does not exist, would you like to login as a new user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        showDialog("Signing up.....");
                        signUp();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void signUp() {

        this.db.collection(DocumentNames.users).document(userID.getText().toString()).set(createNewUser())
                .addOnFailureListener(Login.this)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        cancelDialog();
                        storage.setLogin(true);
                        storage.setTagID(userID.getText().toString().trim());
                        mainActivity.startHomeActivity();
                    }
                });

    }
    private User createNewUser(){
        User user = new User();
        user.setCreatedDate((util.getCurrentTime()).toString());
        user.setEnable("true");
        user.setPassword(passward.getText().toString());
        user.setUsername(userID.getText().toString());
        user.setUserTagId(userID.getText().toString());
//        user.put(DocumentFieldsNames.username,userID.getText().toString());
//        user.put(DocumentFieldsNames.passward,passward.getText().toString());
//        user.put(DocumentFieldsNames.createdDate,(util.getCurrentTime()).toString());
//        user.put(DocumentFieldsNames.enable,"true");
        return user;
    }

    @Override
    public void onFailure(@NonNull Exception e) {

        M.t(mainActivity,"Error");
        e.printStackTrace();
        cancelDialog();

    }


    private void login(String tagId,String passward){

        if(mainActivity.check_network()) {
            this.showDialog("Connecting....");
            DocumentReference documentReference = db.collection(DocumentNames.users).document(tagId);
            documentReference.get().addOnCompleteListener(Login.this);
        }
        else M.t(mainActivity,"Enable internet");

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login:
                if(userID.getText().toString().isEmpty())
                    M.t(mainActivity,"Enter user name");
                else if(passward.getText().toString().isEmpty())
                    M.t(mainActivity,"Enter password");
                else login(userID.getText().toString().trim(),passward.getText().toString().trim());
                break;
        }
    }
    private void showDialog(String message){
        this.progress.setMessage(message);
        this.progress.show();
    }
    private void cancelDialog(){
        if(progress!=null)
            if(progress.isShowing())
                this.progress.dismiss();
    }

}
