package dtrade.habibqureshi.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dtrade.habibqureshi.Ativities.HomeActivity;
import dtrade.habibqureshi.FirebaseDataBase.DocumentNames;
import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;
import dtrade.habibqureshi.Utils.util;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPost extends Fragment implements View.OnClickListener , OnFailureListener {
    private View layout;
    private HomeActivity activity;
    private Spinner shapes;
    private HashMap<String,String> selectedColors,selectedClarity;
    private Button postNow;
    private EditText lowerSize, upperSize, comments;
    private FirebaseFirestore db;
    private LocalStorage storage;
    private ImageView back;



    public NewPost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout= inflater.inflate(R.layout.fragment_new_post, container, false);
        setRetainInstance(true);
        findViews();
        init();
        return layout;
    }

    private void init() {
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(activity, R.array.shapes,
                        android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shapes.setAdapter(staticAdapter);
        postNow.setOnClickListener(this);
        selectedClarity= new HashMap<>();
        selectedColors= new HashMap<>();
        this.db = FirebaseFirestore.getInstance();
        storage= new LocalStorage(activity);
        this.activity.setCurrentFragment(this);
        this.back.setOnClickListener(this);

    }

    private void findViews() {
        shapes=  layout.findViewById(R.id.shapes);
        postNow= layout.findViewById(R.id.postnow);
        lowerSize= layout.findViewById(R.id.lowerLimit);
        upperSize = layout.findViewById(R.id.uperLimit);
        comments= layout.findViewById(R.id.comments);
        back = layout.findViewById(R.id.back);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof HomeActivity)
            activity=(HomeActivity) context;
    }

    public void colorSelect(View v){
        CheckBox checkBox = (CheckBox)v;
        if (checkBox.isChecked())
            selectedColors.put(checkBox.getContentDescription().toString(),checkBox.getContentDescription().toString());
        else
            selectedColors.remove(checkBox.getContentDescription().toString());

    }
    public void claritySelect(View v){
        CheckBox checkBox = (CheckBox)v;
        if (checkBox.isChecked())
            selectedClarity.put(checkBox.getContentDescription().toString(),checkBox.getContentDescription().toString());
        else
            selectedClarity.remove(checkBox.getContentDescription().toString());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.postnow:
                if(activity.check_network())
                    postData();
                else M.t(activity,"Enable internet");
                break;
            case R.id.back:
                activity.removeFragment();
                break;
        }
    }

    private void postData() {
        if(validateSize()) {
            List<String> colors = new ArrayList<>();
            List<String> clarity = new ArrayList();
            if(selectedColors.size()==0)
            {
                M.t(activity,"Select at least 1 color");
                return;
            }
            for (String key : selectedColors.keySet())
                colors.add(key);
            M.l("colors--> " + colors.toString());
            if(selectedClarity.size()==0)
            {
                M.t(activity,"Select at least 1 clarity");
                return;
            }
            for (String key : selectedClarity.keySet())
                clarity.add(key);
            M.l("clarity--> " + clarity.toString());
            if(shapes.getSelectedItem().toString().equals("Select")){
                M.t(activity,"Please select shape");
                return;
            }
            String comments= this.comments.getText().toString();
            List<String> size = new ArrayList();
            size.add(lowerSize.getText().toString());
            size.add(upperSize.getText().toString());
            activity.showDialog("Posting....");
            Post post = new Post();
            post.setClarity(clarity);
            post.setColor(colors);
            post.setComments(comments);
            post.setCreatedDate(util.getCurrentTime());
            post.setShape(shapes.getSelectedItem().toString());
            post.setSize(size);
            postDataToFireBase(post);
        }


    }

    private void postDataToFireBase(Post post){
        this.db.collection(DocumentNames.usersPost).document(storage.getTagID())
                .collection(DocumentNames.post).document()
                .set(post)
                .addOnFailureListener(this)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        activity.cancelDialog();
                        PostSaving.userPosts=PostSaving.myPost=null;
                        M.t(activity,"Posted");
                        activity.changeFragment(new MyPosts(),"MyPosts","MyPosts");


                    }
                });
        HashMap<String,String> p=new HashMap<>();
        p.put("Poest","yes");
        this.db.collection(DocumentNames.usersPost).document(storage.getTagID()).set(p);
        this.activity.sendNotification("all","New Post","newpost",storage.getTagID()+" posted",null);
    }

    private boolean validateSize() {
        String lowerLimit=lowerSize.getText().toString();
        String upperLimit= upperSize.getText().toString();
        if(lowerLimit.isEmpty()) {
            M.t(activity,"Please enter lower limit");
            return false;
        }
        if(upperLimit.isEmpty()) {
            M.t(activity,"Please enter upper limit");
            return false;
        }

        double lower = Double.parseDouble(lowerLimit);
        double upper  = Double.parseDouble(upperLimit);

        if(lower<=upper){
            if(lower>0.01)
            {
                if(upper<=100)
                    return true;
                else
                    M.t(activity,"upper limit should be less or equal to 100");
            }
            else
                M.t(activity,"lower limit should be greater or equal to 0.01");
            }
            else
                M.t(activity,"lower size can't greater then upper size");


        return false;
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        M.t(activity,"Error");
        e.printStackTrace();
        activity.cancelDialog();
    }
}
