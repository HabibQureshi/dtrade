package dtrade.habibqureshi.Interfaces;

import android.view.View;

/**
 * Created by HabibQureshi on 10/21/2017.
 */

public interface OnClickInterface {
    void drawerOnClick(View view, int position);

}
