package dtrade.habibqureshi.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import dtrade.habibqureshi.R;

/**
 * Created by HabibQureshi on 10/17/2017.
 */

public class DrawerListAdapter extends RecyclerView.Adapter<DrawerListAdapter.ViewHolder> {
    View layout;
    LayoutInflater inflater;
    //Bitmap icon;
    String []items={"Home","My posts","My profile","Inbox","Settings","Logout"};
    Context context;
    int Total=items.length;
    int icons[];


    public DrawerListAdapter() {


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            this.inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout=inflater.inflate(R.layout.drawer_adaper_layout,parent,false);
        ViewHolder holder=new ViewHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv.setText(this.items[position]);
        }

    @Override
    public int getItemCount() {
        return Total;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       TextView tv;
       public ViewHolder(View itemView) {
            super(itemView);
            tv=  itemView.findViewById(R.id.tv_NavTitle);


        }


    }
}
