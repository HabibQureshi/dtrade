package dtrade.habibqureshi.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import dtrade.habibqureshi.Models.InboxList;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.PostSaving;
import dtrade.habibqureshi.Utils.util;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {

    private List<InboxList> inboxLists;
    private View layout;
    private LayoutInflater inflater;

    public InboxAdapter(List<InboxList> users) {
        this.inboxLists = users;
    }

    public void setInboxLists(List<InboxList> inboxLists) {
        this.inboxLists = inboxLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = inflater.inflate(R.layout.inbox, parent, false);
        ViewHolder holder = new ViewHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.username.setText(PostSaving.users.get(inboxLists.get(position).getUser()).getUsername());
        holder.time.setText(inboxLists.get(position).getTime());
        holder.message.setText(inboxLists.get(position).getMessage());
        if(PostSaving.users.get(inboxLists.get(position).getUser()).getPicture()!=null)
                holder.iv.setImageBitmap(util.convertBase64ToImage(PostSaving.users.get(inboxLists.get(position).getUser()).getPicture()));
            else holder.iv.setImageResource(R.drawable.default_user);


    }

    @Override
    public int getItemCount() {
        return inboxLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView username,time,message;
        private ImageView iv;


        public ViewHolder(View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.username);
            iv=itemView.findViewById(R.id.userPhoto);
            time = itemView.findViewById(R.id.time);
            message = itemView.findViewById(R.id.message);

        }
    }
}
