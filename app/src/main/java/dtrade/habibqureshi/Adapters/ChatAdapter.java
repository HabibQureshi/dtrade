package dtrade.habibqureshi.Adapters;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dtrade.habibqureshi.Fragments.Chat;
import dtrade.habibqureshi.Models.Message;
import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;
import dtrade.habibqureshi.Utils.util;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>{

    private List<Message> messages;
    private LocalStorage storage;
    private LayoutInflater inflater;
    private View layout;
    private Date d;
    private String user;
    public ChatAdapter(List<Message> messages, Context context,String user) {
        this.messages = messages;
        this.user = user;
        this.storage=new LocalStorage(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = inflater.inflate(R.layout.activity_chat_adapter, parent, false);
        ViewHolder holder = new ViewHolder(layout);
        return holder;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.message.setText(messages.get(position).getMessage());
        d= util.getTime(messages.get(position).getTime());
        if(d != null){
            holder.time.setText(d.getHours()+":"+d.getMinutes());
        }

        if(storage.getTagID().equals(messages.get(position).getUserTagId())){
            M.l(messages.get(position).getMessage() + " " +messages.get(position).getUserTagId() +" align right");
            holder.username.setText("");
            holder.row.setBackgroundResource(R.color.green);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.alignWithParent=true;
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.row.setLayoutParams(params);
        }

        else {
            M.l(messages.get(position).getMessage() + " " +messages.get(position).getUserTagId() +" align left");
            if(PostSaving.users != null)
                holder.username.setText(PostSaving.users.get(messages.get(position).getUserTagId()).getUsername()+": ");
            else
                holder.username.setText(user+": ");
            holder.row.setBackgroundResource(R.color.orange);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.alignWithParent=true;
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            holder.row.setLayoutParams(params);
        }


    }
    public void update(List<Message> messages) {
        this.messages = messages;
    }



    @Override
    public int getItemCount() {
        return this.messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout row;
        private TextView username,message,time;


        public ViewHolder(View itemView) {
            super(itemView);
            row=itemView.findViewById(R.id.row);
            username=itemView.findViewById(R.id.username);
            message=itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }
    }
}
