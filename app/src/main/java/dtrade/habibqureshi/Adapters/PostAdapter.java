package dtrade.habibqureshi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.Models.User;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.util;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> implements View.OnClickListener {
    private View layout;
    private LayoutInflater inflater;
    //Bitmap icon;
    private List<Post> posts;
    private HashMap<String, User> users;
    private Context context;
    private int Total;
    private Boolean showDel;
    private View.OnClickListener clickListener;

    public void setPosts(List<Post> posts, HashMap<String, User> users) {
        this.posts = posts;
        this.Total = posts.size();
        this.users = users;
        M.l("post total " + Total);
    }

    public PostAdapter(Boolean showDeleteButton, View.OnClickListener clickListener) {
        this.showDel = showDeleteButton;
        this.clickListener = clickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = inflater.inflate(R.layout.post_adapter_layout, parent, false);
        ViewHolder holder = new ViewHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.postData.setText(Html.fromHtml(this.posts.get(position).getShape()+" | " + this.posts.get(position).getSize().get(0)+"ct - "+this.posts.get(position).getSize().get(1)+"ct | " +
                util.getListAsString(this.posts.get(position).getColor()) + " | " + util.getListAsString(this.posts.get(position).getClarity()) + "<br><span  style=\"color:red;\">"+this.posts.get(position).getComments()+"</span>"));
//        holder.shape.setText(this.posts.get(position).getShape());
//        holder.comments.setText(this.posts.get(position).getComments());
//        holder.size.setText(this.posts.get(position).getSize().get(0)+" to "+this.posts.get(position).getSize().get(1));
//        holder.color.setText(util.getListAsString(this.posts.get(position).getColor()));
//        holder.clarity.setText(util.getListAsString(this.posts.get(position).getClarity()));
        holder.postTime.setText(posts.get(position).getCreatedDate());
        if (showDel) {
            holder.deletePost.setContentDescription(Integer.toString(position));
            holder.deletePost.setOnClickListener(clickListener);
        } else {
            holder.userName.setText(this.users.get(posts.get(position).getUser()).getUsername());

            if(users.get(posts.get(position).getUser()).getPicture()!=null)
                holder.userPic.setImageBitmap(util.convertBase64ToImage(users.get(posts.get(position).getUser()).getPicture()));
            else holder.userPic.setImageResource(R.drawable.default_user);
        }

    }

    @Override
    public int getItemCount() {
        return Total;
    }

    @Override
    public void onClick(View v) {
        posts.remove(Integer.parseInt(v.getContentDescription().toString()));
        M.l(v.getContentDescription().toString() + " delete this post");
    }

    public void removeItem(int i) {
        this.posts.remove(i);
        this.Total--;
    }

    public String getPostId(int i) {
        return this.posts.get(i).getPostId();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //ImageView iv;
        TextView shape, size, color, clarity, comments, postTime, userName,postData;
        private ImageView deletePost, userPic;


        public ViewHolder(View itemView) {
            super(itemView);
            postData =  itemView.findViewById(R.id.postdata);
//            shape = itemView.findViewById(R.id.shape);
//            size = itemView.findViewById(R.id.size);
//            color = itemView.findViewById(R.id.color);
//            clarity = itemView.findViewById(R.id.clarity);
//            comments = itemView.findViewById(R.id.comments);
            deletePost = itemView.findViewById(R.id.deletePost);
            postTime = itemView.findViewById(R.id.postTime);
            userName = itemView.findViewById(R.id.username);
            userPic = itemView.findViewById(R.id.userPic);

            if (!showDel) {
                deletePost.setVisibility(View.GONE);


            } else {
                //postTime.setVisibility(View.GONE);
                userName.setVisibility(View.GONE);
                userPic.setVisibility(View.GONE);
            }

        }


    }
}
