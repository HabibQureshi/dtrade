package dtrade.habibqureshi.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class util {

    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }
    public static String getListAsString(List<String> list){
        String listString="";
        for (int i=0;i<list.size();i++)
        {
            if(i==(list.size()-1))
                listString+= list.get(i);
            else
                listString+= list.get(i)+" - ";

        }
        return listString;
    }

    public static Bitmap convertBase64ToImage(String picture) {
        byte[] decodedString = Base64.decode(picture, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }

    public static Date getTime(String time) {

        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD HH:MM:SS", Locale.ENGLISH);
            Date date = dateFormat.parse(time);
            return date;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }



}
