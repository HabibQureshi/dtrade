package dtrade.habibqureshi.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import dtrade.habibqureshi.Models.Post;
import dtrade.habibqureshi.Models.User;

import static android.content.Context.MODE_PRIVATE;

public class LocalStorage {

    private String login ="login";
    private String tagID="tag";
    private String postAlert="postAlter";
    private String messageAlert="messageAlert";
    public static List<Post> posts;
    public static User user;

    public static List<Post> getPosts() {
        return posts;
    }

    public static void setPosts(List<Post> posts) {
        LocalStorage.posts = posts;
    }

    private Context context;
    private SharedPreferences pref;
    private final String MY_PREFS_NAME = "DTrade";

    public LocalStorage(Context context) {
        this.context = context;
        pref=context.getSharedPreferences(MY_PREFS_NAME,MODE_PRIVATE);

    }

    public void setLogin(boolean login){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(this.login,login);
        editor.apply();
        }

    public String getTagID() {
        return pref.getString(this.tagID,"");
    }

    public void setTagID(String tagID) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(this.tagID,tagID);
        editor.apply();
    }

    public boolean getLogin(){
       return pref.getBoolean(this.login,false);
    }
    public boolean getPostAlert(){
        return pref.getBoolean(this.postAlert,true);

    }

    public boolean getMessageAlert(){
        return pref.getBoolean(this.messageAlert,true);
    }

    public void setMessageAlert(boolean alert){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(this.messageAlert,alert);
        editor.apply();
    }

    public void setPostAlert(boolean alert){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(this.postAlert,alert);
        editor.apply();
    }

}
