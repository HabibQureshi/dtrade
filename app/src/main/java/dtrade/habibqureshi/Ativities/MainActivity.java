package dtrade.habibqureshi.Ativities;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import dtrade.habibqureshi.Fragments.Login;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;

public class MainActivity extends AppCompatActivity {

    private String TAG =this.getClass().getSimpleName();
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private LocalStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if(storage.getLogin())
            startHomeActivity();
        else{

            fragment = new Login();
            this.changeFragment(fragment);
        }

    }

    private void changeFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.main_content, fragment)
                .commit();
    }
    private void init (){
        fragmentManager = getSupportFragmentManager();
        storage = new LocalStorage(this);
    }

    public void startHomeActivity(){
        startActivity(new Intent(this,HomeActivity.class));
        finish();

    }

    public boolean check_network() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

}
