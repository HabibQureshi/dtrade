package dtrade.habibqureshi.Ativities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.messaging.FirebaseMessaging;

import dtrade.habibqureshi.Fragments.Chat;
import dtrade.habibqureshi.Fragments.Home;
import dtrade.habibqureshi.Fragments.Inbox;
import dtrade.habibqureshi.Fragments.MyPosts;
import dtrade.habibqureshi.Fragments.MyProfile;
import dtrade.habibqureshi.Fragments.Navigation;
import dtrade.habibqureshi.Fragments.NewPost;
import dtrade.habibqureshi.Fragments.Setting;
import dtrade.habibqureshi.Interfaces.OnClickInterface;
import dtrade.habibqureshi.Interfaces.OnJsonResponse;
import dtrade.habibqureshi.Network.GetJsonResponse;
import dtrade.habibqureshi.R;
import dtrade.habibqureshi.Utils.LocalStorage;
import dtrade.habibqureshi.Utils.M;
import dtrade.habibqureshi.Utils.PostSaving;

public class HomeActivity extends AppCompatActivity implements OnClickInterface,OnJsonResponse{

    private Toolbar toolbar;
    private Navigation navigation;
    private Fragment fragment;
    private LocalStorage storage;
    private FragmentManager fragmentManager;
    private ProgressDialog progress;
    private TextView tittle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        M.l("on create");
        setContentView(R.layout.activity_home);
        initFindViewsById();
        init();
        if(getIntent().getExtras()!=null){
            Bundle bundle1 = getIntent().getExtras();
            if (bundle1 != null) {
                for (String key : bundle1.keySet()) {
                    Object value = bundle1.get(key);
                    M.l("Extra = "+key);
                    M.l("Extra = "+value);
                }
            }
            M.l("intent extras not null");
            if((getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equals("newpost"))||getIntent().getExtras().get("newpost")!=null){
                M.l("new post intent extra");
                PostSaving.userPosts=null;
                changeFragment(new Home(),"Home","Home");
            }

            else if((getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equals("newmessage")) || getIntent().getExtras().get("newmessage")!=null){
                Chat chat = new Chat();
                M.l("new message intent extra");
                Bundle bundle = new Bundle();
                M.l("sender "+getIntent().getExtras().getString("s"));
                bundle.putString("user",getIntent().getExtras().getString("s"));
                chat.setArguments(bundle);
                changeFragment(chat,getIntent().getExtras().getString("s"),getIntent().getExtras().getString("s"));

            }
        }
        else

        if(savedInstanceState != null)
        {
            M.l("savedInstanceState ");
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, "currentFragment");
            changeFragment(fragment,fragment.getTag(),fragment.getTag());
        }
        else
        changeFragment(new Home(),"Home","Home");
    }
    public void initFindViewsById() {
        toolbar =  findViewById(R.id.include_toolbar);
        tittle = toolbar.findViewById(R.id.tittle);
        navigation = (Navigation) getSupportFragmentManager().findFragmentById(R.id.nav);

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getExtras().getString("tittle").equals("New Post"))
            {
                if(intent.getExtras().getString("user").equals(storage.getTagID()))
                    M.l("Same user");
                else {
                    notifyUserAboutNewPost(intent);
                    M.l("new Post from " + intent.getExtras().getString("user"));
                }
            }
            else if(intent.getExtras().getString("tittle").equals("New Message"))
            {
                if(fragment !=null)
                {
                    if(fragment instanceof Chat)
                    {
                        if(((Chat) fragment).getUser().equals(intent.getExtras().getString("s")))
                            return;
                    }
                }
                notifyUserAboutNewMessage(intent);

            }
            else
                showAdminMessage(intent);
        }
    };

    private void notifyUserAboutNewMessage(Intent intent) {
        M.l("Creating message notification");
        Intent intent1 = new Intent(HomeActivity.this, HomeActivity.class);
        intent1.putExtra("newmessage","yes");
        intent1.putExtra("s",intent.getExtras().getString("s"));
        intent1.setAction("new");
        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(HomeActivity.this, Integer.parseInt(intent.getExtras().getString("s")), intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(HomeActivity.this, intent.getExtras().getString("s"))
                .setSmallIcon(R.drawable.default_user)
                .setContentTitle(intent.getExtras().get("tittle").toString())
                .setContentText(intent.getExtras().get("message").toString())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(HomeActivity.this);
        notificationManager.notify(Integer.parseInt(intent.getExtras().getString("s")), mBuilder.build());
    }

    private void notifyUserAboutNewPost(Intent intent){
        Intent intent1 = new Intent(HomeActivity.this, HomeActivity.class);
        intent1.putExtra("newpost","yes");
        intent1.setAction("new");
        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(HomeActivity.this, 0, intent1, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(HomeActivity.this, "1")
                .setSmallIcon(R.drawable.default_user)
                .setContentTitle(intent.getExtras().get("tittle").toString())
                .setContentText(intent.getExtras().get("message").toString())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(HomeActivity.this);
        notificationManager.notify(1, mBuilder.build());
    }

    private void showAdminMessage(Intent intent) {
        AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("Admin")
                    .setMessage(intent.getExtras().getString("message"))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
    }

    public void init() {

        fragmentManager = getSupportFragmentManager();
        storage = new LocalStorage(this);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        progress=new ProgressDialog(this);
        this.progress.setCanceledOnTouchOutside(false);
        navigation.setUp(R.id.nav, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar, this, this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("notification"));
        subscribeToTopics();

        }
    public void changeFragment(Fragment fragment,String Tag,String Tittle) {
        hideKeyoard();

        if(this.fragment == null || !this.fragment.getTag().equals(Tag))
        {
            FragmentTransaction transaction=fragmentManager.beginTransaction();
            transaction.replace(R.id.main_content, fragment,Tag);
            transaction.addToBackStack(null);
            transaction.commit();
            this.fragment=fragment;

        }


        //setTittle(Tittle);

       // if(fragment.getTag()=="Home")
       //     navigation.showHamburgerIcon();
        //else
          //  navigation.showBackIcon();
    }
    public void setTittle(String tittle){
       this.tittle.setText(tittle);

    }

    public void removeFragmentFromStack(){
       this.removeFragment();

    }

    public void removeFragment(){
        hideKeyoard();
        M.l("current fragment"+fragment.getTag()+"");
        if(fragment.getTag()=="Home"){
            if(((Home)fragment).isDataFilter()){
                ((Home)fragment).setDataFilter(false);
                ((Home)fragment).showAllPost();
            }
            else
                finish();
        }

        else
        if (fragmentManager.getBackStackEntryCount() > 0)
            fragmentManager.popBackStackImmediate();
        else super.onBackPressed();
        M.l("current fragment"+fragment.getTag()+"");
//        if(fragment.getTag()=="Home")
//            navigation.showHamburgerIcon();
        if(fragment.getTag() == null)
            finish();
       // else tittle.setText(fragment.getTag());
    }

    @Override
    public void onBackPressed() {
        if(navigation.isDrawerOpen())
            navigation.closeDrawer();
        else
        this.removeFragment();
        }

    public void setCurrentFragment(Fragment fragment)
    {
        this.fragment= fragment;
    }


    @Override
    public void drawerOnClick(View view, int position) {
        this.navigation.closeDrawer();
        hideKeyoard();
       switch (position){
           case 0:
               this.changeFragment(new Home(),"Home","Home");
              // navigation.showHamburgerIcon();
               break;

           case 1:
               this.changeFragment(new MyPosts(),"MyPosts","MyPosts");
              // navigation.showBackIcon();

               break;
           case 2:
               this.changeFragment(new MyProfile(),"MyProfile","MyProfile");
               //navigation.showBackIcon();
               break;
           case 3:
               this.changeFragment(new Inbox(),"Inbox","Inbox");
               //navigation.showBackIcon();
               break;
           case 4:
               this.changeFragment(new Setting(),"Settings","Settings");
               break;
           case 5:
               logout();
               break;
       }


    }

    private void logout() {
        unSubscribeToTopic();
        this.storage.setLogin(false);
        this.storage.setTagID(null);
        this.storage.setMessageAlert(true);
        this.storage.setPostAlert(true);
        PostSaving.users=null;
        PostSaving.myPost=null;
        PostSaving.userPosts=null;
        LocalStorage.user=null;
        PostSaving.inbox=null;
        startActivity(new Intent(HomeActivity.this,MainActivity.class));
        finish();
    }

    public void colorSelect(View v){
        M.l("activty");
        if(fragment instanceof NewPost){
           ((NewPost)fragment).colorSelect(v);
       }
       else  M.l("no");
    }
    public void claritySelect(View v){
        M.l("activty");
        if(fragment instanceof NewPost){
            ((NewPost)fragment).claritySelect(v);
        }
        else  M.l("no");
    }
    public void settings(View v){
        if(fragment instanceof Setting)
            ((Setting) fragment).settings(v);
    }
    public void showDialog(String message){
        this.progress.setMessage(message);
        this.progress.show();
    }
    public void cancelDialog(){
        M.l("cancel dailog");
        if(progress!=null)
            if(progress.isShowing())
                this.progress.dismiss();
    }
    public boolean check_network() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("tittle",getSupportActionBar().getTitle().toString());
        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }

    @Override
    public void onJsonResponse(String response, int RequestType) {
        M.l(response+"");
    }

    public void sendNotification(String topic, String tittle, String type, String body, String user){
        String json="{" +
                "  \"to\": \"/topics/"+topic+"\"," +
                "  \"notification\" : {" +
                "     \"body\" : \" "+ body+"\"," +
                "     \"title\": \""+tittle+"\"" +
                "     \"click_action\":\"HOME_ACTIVITY\","+
                " },    " +
                " \"data\": {" +
                "    \"user\": "+storage.getTagID()+"," +
                "      \"type\":\""+type+"\"" +
                "      \"s\":\""+storage.getTagID()+"\"" +
                "   }" +
                "}";
        M.l("Json--> "+json);
        new GetJsonResponse(this,0,json).execute("https://fcm.googleapis.com/fcm/send");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        hideKeyoard();
        M.l("new Intent" + intent.toString());
       // this.setIntent(intent);

        if(intent.getExtras().get("newpost")!=null ){
            M.l("new post");
            PostSaving.userPosts=null;
            changeFragment(new Home(),"Home","Home");
        }

        else if((intent.getExtras().get("type")!=null && intent.getExtras().get("type").equals("newmessage")) || intent.getExtras().get("newmessage")!=null)
        {
            Chat chat = new Chat();
            Bundle bundle = new Bundle();
            M.l(intent.getExtras().getString("s")+" this is sender");
            bundle.putString("user",intent.getExtras().getString("s"));
            chat.setArguments(bundle);
            changeFragment(chat,intent.getExtras().getString("s"),intent.getExtras().getString("s"));
        }

        else M.l("not new Post");




    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        M.l("On destory");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    public void subscribeToTopics(){

        subscribeToPostNotification();
        subscribeToMessageNotification();


    }

    public void unSubscribeToTopic(){
        unSubscribeToMessageNotification();
        unSubscribeToPostNotification();

    }

    public void subscribeToPostNotification(){
        FirebaseMessaging.getInstance().subscribeToTopic("all");
    }
    public void subscribeToMessageNotification(){
        FirebaseMessaging.getInstance().subscribeToTopic(storage.getTagID());
    }
    public void unSubscribeToPostNotification(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic("all");
    }
    public void unSubscribeToMessageNotification(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(storage.getTagID());
    }

    public void hideKeyoard(){
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }
}
